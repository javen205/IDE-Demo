package com.javen.common;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.core.JFinal;
import com.jfinal.kit.StrKit;
import com.oreilly.servlet.multipart.FileRenamePolicy;

/**
 * JFinal2.2文件上传重命名策略
 */
public class UpFileRenamePolicy implements FileRenamePolicy {
 
    @Override
    public File rename(File f) {
        // 获取webRoot目录
//        String webRoot = PathKit.getWebRootPath();
        // 用户设置的默认上传目录
        String saveDir = JFinal.me().getConstants().getBaseUploadPath();
        // 添加时间作为目录
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
 
        // /xxx/项目名称/upload/[日期]/1474451862845.jgp
        StringBuilder newFileName = new StringBuilder()
            .append(StrKit.isBlank(saveDir) ? "upload" : saveDir)
            .append(File.separator)
            .append(dateFormat.format(new Date()))
            .append(File.separator)
            .append(System.currentTimeMillis())
            .append(getFileExt(f.getName()));
 
        File dest = new File(newFileName.toString());
        // 创建上层目录
        File dir = dest.getParentFile();
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dest;
    }
 
    /**
     * 获取文件后缀
     * 
     * @param @param fileName
     * @param @return 设定文件
     * @return String 返回类型
     */
    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf('.'), fileName.length());
    }
}