package com.javen.common.hander;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javen.common.entity.UserAgent;
import com.javen.common.utils.UserAgentUtil;
import com.jfinal.handler.Handler;
import com.jfinal.log.Log;

public class UserAgentHandler extends Handler {
    protected final Log log = Log.getLog(getClass());

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		String userAgent = request.getHeader("User-Agent");
		log.info(userAgent);
		UserAgent ua = UserAgentUtil.getUserAgent(userAgent);
        next.handle(target, request, response, isHandled);
        log.info(ua.toString());
	}

}
