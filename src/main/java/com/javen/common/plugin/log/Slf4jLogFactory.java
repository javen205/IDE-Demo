package com.javen.common.plugin.log;

import com.jfinal.log.ILogFactory;
import com.jfinal.log.Log;
/**
 * 用Slf4j替换Log4j进行日志处理
 */
public class Slf4jLogFactory implements ILogFactory {

	@Override
	public Log getLog(Class<?> clazz) {
		return new Slf4jLog(clazz);
	}

	@Override
	public Log getLog(String name) {
		return new Slf4jLog(name);
	}
}
