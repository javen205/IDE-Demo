package com.javen.common.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
public class CSVUtils {

	private final static String charsetName = "GB2312";
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	/**
	 * 导出
	 * 
	 * @param file
	 * @param dataList
	 * @return
	 */
	public static boolean exportCsv(String filePath, List<List<Object>> dataList) {
		return exportCsv(new File(filePath), dataList);
	}

	public static boolean exportCsv(File file, List<List<Object>> dataList) {
		boolean isSucess = false;

		FileOutputStream out = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;
		try {
			out = new FileOutputStream(file);
			osw = new OutputStreamWriter(out, charsetName);
			bw = new BufferedWriter(osw, 1024);
			if (dataList != null && !dataList.isEmpty()) {
				for (List<Object> rowData : dataList) {
					if (rowData != null && !rowData.isEmpty()) {
						for (int i = 0; i < rowData.size(); i++) {
							Object data = rowData.get(i);
							if (null != data) {
								if (data instanceof Date) {
									bw.append(dateFormat.format(data));
								} else {
									bw.append(data.toString());
								}
							}
							if (i < rowData.size() - 1) {
								bw.append(",");
							}
						}
					}

					bw.append("\r");
				}
			}
			isSucess = true;
		} catch (Exception e) {
			isSucess = false;
			e.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
					bw = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (osw != null) {
				try {
					osw.close();
					osw = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return isSucess;
	}

	/**
	 * 导入
	 * 
	 * @param file
	 * @return
	 */
	public static List<String> importCsv(File file) {
		List<String> dataList = new ArrayList<String>();

		FileInputStream inp = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try {
			inp = new FileInputStream(file);
			isr = new InputStreamReader(inp, charsetName);
			br = new BufferedReader(isr, 1024);

			String line = "";
			while ((line = br.readLine()) != null) {
				dataList.add(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
					br = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return dataList;
	}

	public static void main(String[] args) {
		List<List<Object>> dataList = new ArrayList<List<Object>>();
		dataList.add(new ArrayList<Object>() {
			private static final long serialVersionUID = 1L;
			{
				add(1);
				add("张三");
				add("男");
			}
		});
		dataList.add(new ArrayList<Object>() {
			private static final long serialVersionUID = 1L;
			{
				add(2);
				add("李四");
				add(new Date());
			}
		});
		dataList.add(new ArrayList<Object>() {
			private static final long serialVersionUID = 1L;
			{
				add(3);
				add("小红");
				add("女");
			}
		});

		boolean isSuccess = CSVUtils.exportCsv(new File("D:/test.csv"), dataList);
		System.out.println(isSuccess);

		List<String> dataList2 = CSVUtils.importCsv(new File("D:/test.csv"));
		if (dataList2 != null && !dataList2.isEmpty()) {
			for (String data : dataList2) {
				System.out.println(data);
			}
		}
	}
}
