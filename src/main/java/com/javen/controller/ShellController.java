package com.javen.controller;

import java.util.List;

import com.javen.kit.ShellKit;
import com.jfinal.core.Controller;

public class ShellController extends Controller {
	
	public void index(){
		String shell = getPara("shell");
		ShellKit.execShell(shell);
		renderText("执行完成...");
	}
	
	
	public void runShell(){
		String shStr = getPara("shell");
		try {
			List<String> list = ShellKit.runShell(shStr);
			renderJson(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
}
