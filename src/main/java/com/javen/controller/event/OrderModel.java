package com.javen.controller.event;

public class OrderModel {
	private int id;
	private String name;
	
	public OrderModel() {
	}
	
	public OrderModel(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "OrderModel [id=" + id + ", name=" + name + "]";
	}
	
	
}
