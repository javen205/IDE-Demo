package com.javen.controller.event;

import com.jfinal.core.Controller;

import net.dreamlu.event.EventKit;

public class JfinalEventController extends Controller {
	
	public void index(){
		OrderModel orderModel = new OrderModel(1, "test");
		EventKit.post(new OrderEvent(orderModel));
		EventKit.post("save", new OrderEvent(orderModel));
		renderText("发送事件...");
	}
}
