package com.javen.controller.event;

import net.dreamlu.event.core.ApplicationListener;
import net.dreamlu.event.core.Listener;

@Listener(order = 1, enableAsync = true, tag="save")
public class OrderSMSListener implements ApplicationListener<OrderEvent> {

    @Override
    public void onApplicationEvent(OrderEvent event) {
        OrderModel order = (OrderModel) event.getSource();

        System.out.println("OrderSMSListener>"+order.toString());
    }

}