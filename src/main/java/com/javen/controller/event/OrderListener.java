package com.javen.controller.event;

import net.dreamlu.event.core.ApplicationListener;
import net.dreamlu.event.core.Listener;

@Listener(order = 2, enableAsync = true, tag="save")
public class OrderListener implements ApplicationListener<OrderEvent> {

    @Override
    public void onApplicationEvent(OrderEvent event) {
        OrderModel order = (OrderModel) event.getSource();

        System.out.println("OrderListener>"+order.toString());
    }

}