package com.javen.controller.event;

import net.dreamlu.event.core.ApplicationEvent;

public class OrderEvent extends ApplicationEvent {

	private static final long serialVersionUID = -6497268316815199541L;

	public OrderEvent(Object source) {
        super(source);
    }

}