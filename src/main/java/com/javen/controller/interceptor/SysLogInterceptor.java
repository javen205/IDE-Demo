
package com.javen.controller.interceptor;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.javen.common.utils.CommonUtils;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/**
 * 日志拦截器
 */
public class SysLogInterceptor implements Interceptor {
	
	public void intercept(Invocation inv) {
		String from=inv.getController().getRequest().getHeader("Referer");
		String ip=CommonUtils.getIP(inv.getController().getRequest());
		String className=inv.getController().getClass().getName();
		String methodName=inv.getMethodName();
		Integer uid=0;
//		SysUser sysUser=IWebUtils.getCurrentSysUser(inv.getController().getRequest());
//		if(sysUser!=null){uid=sysUser.getInt("id");}
		
		long startTime=System.currentTimeMillis();
		try{
			inv.invoke();
			System.out.println(uid+" "+ip+" "+from +" "+ inv.getActionKey()+" "+className+" "+methodName+" "+startTime);
//			SysLog.dao.saveSysLog(uid+" "+ip, from, inv.getActionKey(), className, methodName, startTime, 0,"");
		}catch(RuntimeException e){
			System.out.println(uid+" "+ ip+" "+from+" "+inv.getActionKey()+" "+className+" "+methodName+" "+startTime+" "+-1+" "+getExceptionAllinformation(e));
//			SysLog.dao.saveSysLog(uid+" "+ip, from, inv.getActionKey(), className, methodName, startTime, -1,getExceptionAllinformation(e));
		}
	}
	 /**
	  * 获取异常详细信息
	  */
	  private static String getExceptionAllinformation(Throwable e){   
          StringWriter sw = new StringWriter();   
          PrintWriter pw = new PrintWriter(sw, true);   
          e.printStackTrace(pw);   
          pw.flush();   
          sw.flush();   
          return sw.toString();   
	  } 
}
