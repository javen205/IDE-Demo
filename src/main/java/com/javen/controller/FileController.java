package com.javen.controller;

import java.io.File;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.upload.UploadFile;

/**
 * @author Javen
 */
public class FileController extends Controller {
	public void index(){
		render("file.jsp");
	}
	
	public void add(){
		StringBuffer sbf=new StringBuffer();
		List<UploadFile> files = getFiles();
		String uploadPath = null;
		if (null!=files && files.size()>0) {
			//获取长传文件的路径
			sbf.append(files.get(0).getUploadPath())
			.append("/"+files.get(0).getFileName());
		}
		renderText(sbf.toString());
	}
}
