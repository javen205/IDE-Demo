package com.javen.controller;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;

/**
 * Created by Javen on 16/8/20.
 */
public class IndexController extends Controller {
    static Log logger = Log.getLog(IndexController.class);
    public void index(){
        render("index.jsp");
    }
    
    public void bolg(){
        logger.error("这是我的博客------------");
        redirect("http://javen205.oschina.io");
    }

    public void test(){
        try {
        	int i = 1/0;
            Thread.sleep(1000*5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        renderText("this is test");
    }
    
    public void video(){
    	render("/sdfsdf/video.jsp");
    }
    
    public void weui(){
    	logger.info("此版本号为:V0.8.2>http://jqweui.com");
    	renderNull();
    }
    public void wskt(){
    	render("websocket.jsp");
    }
}
