package com.javen.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javen.model.User;
import com.javen.service.UserSerivce;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;

public class UserController extends Controller {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);   
	
	UserSerivce serivce = UserSerivce.me;
	
	 public void getAllUser(){
    	List<User> all = User.dao.getAllUser();
    	if (all!=null && all.size()>0) {
    		User user = User.dao.getUserById(all.get(0).getId());
    		getSession().setAttribute("user", user);
    		System.out.println(JsonKit.toJson(user));
    		logger.error("slf4j>"+JsonKit.toJson(user));
		}
    	renderJson(all);
    }
	 
	 /**
	  * 通过外部Sql查询
	  */
	 public void getUsers(){
		 List<User> allUser = serivce.getAllUser();
		 renderJson(allUser);
	 }
	 public void getUser(){
		 String id = getPara("id");
		 User user = serivce.getUserById(id);
		 renderJson(user);
	 }
}
