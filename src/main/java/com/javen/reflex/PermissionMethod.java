package com.javen.reflex;

/**
 * Created by Javen on 16/10/3.
 */
public class PermissionMethod {
    public PermissionMethod() {
    }

    @PermissionSuccess(requestCode = 10)
    public void success(){
        System.out.println("success 10");
    }

    @PermissionSuccess(requestCode = 12)
    public void doSomething(){
        System.out.println("doSomething 12");
    }
    @PermissionFail(requestCode = 10)
    public void fail(){
        System.out.println("fail 10");
    }

    @PermissionFail(requestCode = 12)
    public void doFailSomething(){
        System.out.println("doFailSomething 11");
    }

    private int add(int a,int b){
        return a+b;
    }

    public int add(){
        return 3;
    }
}
