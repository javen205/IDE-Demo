package com.javen.reflex;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Javen on 16/10/3.
 */
public class Test {


    private  static void executeSuccess(Object object,int requestCode){

        Method executeMethod = Utils.findMethodWithRequestCode(object.getClass(), PermissionSuccess.class, requestCode);
        if (executeMethod!=null){
            System.out.printf(executeMethod.getName());
        }
        Utils.executeMethod(object,executeMethod);
    }
    private  static void executeFail(Object object,int requestCode){

        Method executeMethod = Utils.findMethodWithRequestCode(object.getClass(), PermissionFail.class, requestCode);
        if (executeMethod!=null){
            System.out.printf(executeMethod.getName());
        }
        Utils.executeMethod(object,executeMethod);
    }

    public static void main(String[] args){

        PermissionMethod permissionMethod = new PermissionMethod();
        Class<? extends PermissionMethod> pmClass = permissionMethod.getClass();
        Method[] methods = pmClass.getMethods();
        for (Method method:methods) {
            System.out.println(method.getName());
        }

        System.out.println("==========执行符合条件的注解方法=================");
        executeSuccess(permissionMethod,10);
        executeSuccess(permissionMethod,12);

        executeFail(permissionMethod,10);
        executeFail(permissionMethod,12);

        System.out.println("==========所有注解方法=================");
        //获取所有带PermissionSuccess注解的方法
        List<Method> successMethods = Utils.findAnnotationMethods(PermissionMethod.class, PermissionSuccess.class);
        if (!successMethods.isEmpty()){
            for (Method method : successMethods) {
                System.out.println(method.getName());
            }
        }

        //获取所有带PermissionFail注解的方法
        List<Method> failMethods = Utils.findAnnotationMethods(PermissionMethod.class, PermissionFail.class);
        if (!successMethods.isEmpty()){
            for (Method method : failMethods) {
                System.out.println(method.getName());
            }
        }

    }


}
