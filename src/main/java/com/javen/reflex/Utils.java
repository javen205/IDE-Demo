package com.javen.reflex;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javen on 16/10/3.
 */
public class Utils {

    public static List<Method> findAnnotationMethods(Class clazz, Class<? extends Annotation> clazz1){
        List<Method> methods = new ArrayList<>();
        for(Method method : clazz.getDeclaredMethods()){
            if(method.isAnnotationPresent(clazz1)){
                methods.add(method);
            }
        }
        return methods;
    }

    public static <A extends Annotation> Method findMethodPermissionFailWithRequestCode(Class clazz, Class<A> permissionFailClass, int requestCode) {
        for(Method method : clazz.getDeclaredMethods()){
            if(method.isAnnotationPresent(permissionFailClass)){
                if(requestCode == method.getAnnotation(PermissionFail.class).requestCode()){
                    return method;
                }
            }
        }
        return null;
    }

    public static <A extends Annotation> Method findMethodPermissionSuccessWithRequestCode(Class clazz,
                                                                                           Class<A> permissionFailClass, int requestCode) {
        for(Method method : clazz.getDeclaredMethods()){
            if(method.isAnnotationPresent(permissionFailClass)){
                if(requestCode == method.getAnnotation(PermissionSuccess.class).requestCode()){
                    return method;
                }
            }
        }
        return null;
    }

    public static <A extends Annotation> Method findMethodWithRequestCode(Class clazz, Class<A> annotation, int requestCode) {
        for(Method method : clazz.getDeclaredMethods()){
            if(method.isAnnotationPresent(annotation)){
                if(isEqualRequestCodeFromAnntation(method, annotation, requestCode)){
                    return method;
                }
            }
        }
        return null;
    }

    public static boolean isEqualRequestCodeFromAnntation(Method m, Class clazz, int requestCode){
        if(clazz.equals(PermissionFail.class)){
            return requestCode == m.getAnnotation(PermissionFail.class).requestCode();
        } else if(clazz.equals(PermissionSuccess.class)){
            return requestCode == m.getAnnotation(PermissionSuccess.class).requestCode();
        } else {
            return false;
        }
    }


    public static void executeMethod(Object activity, Method executeMethod) {
        if(executeMethod != null){
            try {
                if(!executeMethod.isAccessible()) executeMethod.setAccessible(true);
                executeMethod.invoke(activity, null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }else {
            System.out.println("executeMethod is null");
        }
    }
}
