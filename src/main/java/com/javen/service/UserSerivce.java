package com.javen.service;

import java.util.List;

import com.javen.model.User;
import com.jfinal.plugin.activerecord.SqlPara;

public class UserSerivce {
	public static final UserSerivce me = new UserSerivce();
	
	private User userDao = new User().dao();
	
	public List<User> getAllUser(){
		SqlPara sqlPara = userDao.getSqlPara("user.getAllUser");
		List<User> allUser =  userDao.find(sqlPara.getSql(), sqlPara.getPara());
		return allUser;
	}
	
	public User getUserById(String id){
		SqlPara sqlPara = userDao.getSqlPara("user.getUserById",id);
		return userDao.findFirst(sqlPara.getSql(), sqlPara.getPara());
	}
}
