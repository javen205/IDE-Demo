package com.javen.test;

import com.javen.model.User;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
/**
 * 非 web 环境下使用 ActiveRecord
 * @author Javen
 * 2016年11月12日
 */
public class ActiveRecordTest {
	static Log log = Log.getLog(ActiveRecordTest.class);
	public static void main(String[] args) {
		
		PropKit.use("javen_config.txt");
		
		String jdbcUrl = PropKit.get("jdbcUrl");
		String user = PropKit.get("user");
		String password = PropKit.get("password");
		
		log.info(jdbcUrl + " " + user + " " + password);
		DruidPlugin dp = new DruidPlugin(jdbcUrl, user, password);

		ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
		arp.addMapping("user", User.class);

		// 与web环境唯一的不同是要手动调用一次相关插件的start()方法 
		dp.start();
		arp.start();
		// 通过上面简单的几行代码，即可立即开始使用
		new User().set("name", "name").set("sex", "t").save();
		log.info(JsonKit.toJson(User.dao.getUserById(1)) );
	}

}
